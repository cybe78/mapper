#ifndef COMPILATIONUNITHEADER_H
#define COMPILATIONUNITHEADER_H

namespace Typemock
{
namespace Linux
{
namespace Details
{

class CCompilationUnitHeader: private boost::noncopyable
{
public:
    CCompilationUnitHeader(Dwarf_Debug dbg);
    bool MoveToNext();
    Dwarf_Die GetSibling() const;

private:
    Dwarf_Debug m_Dbg;
    Dwarf_Unsigned m_Next;
};

} // namespace Details
} // namespace Linux
} // namespace Typemock

#endif // COMPILATIONUNITHEADER_H
