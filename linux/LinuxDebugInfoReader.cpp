#include "Precompiled.h"
#include "LinuxDebugInfoReader.h"

#include "DebugInfo.h"
#include "Exceptions.h"
#include "linux/details/CompilationUnitHeader.h"
#include "linux/details/DebugInfoEntry.h"

using Typemock::Linux::Details::CCompilationUnitHeader;
using Typemock::EMapperException;
using Typemock::Linux::Details::CDebugInfoEntry;
using Typemock::CDebugInfo;

#define USE_RECURSION

////////////////////////////////////////////////////////////////////

static int OpenFile(const std::string &sFileName)
{
    const int nFileDescriptor = open(sFileName.c_str(), O_RDONLY);
    if (nFileDescriptor < 0) {
        std::ostringstream ss;
        ss << "Could not open file: " << sFileName;
        throw EMapperException(ss.str());
    }
    return nFileDescriptor;
}

////////////////////////////////////////////////////////////////////

static Dwarf_Debug DwarfInit(int nFileDescriptor)
{
    Dwarf_Debug dbg;
    Dwarf_Error err;

    if (dwarf_init(nFileDescriptor, DW_DLC_READ, nullptr, nullptr, &dbg, &err) != DW_DLV_OK) {
        throw EMapperException("Failed DWARF initialization\n");
    }
    return dbg;
}

////////////////////////////////////////////////////////////////////

#if defined(USE_RECURSION)

static void ProcessDie(const boost::optional<CDebugInfoEntry>& rDieOpt, CDebugInfo& rDebugInfo, const std::string& sParentName)
{
    //This is a tail recursion; thus, the stack overflow cannot happen
    if (!rDieOpt) {
        return;
    }
    std::string sName = sParentName;
    rDieOpt->AddInfo(rDebugInfo, sName);
    ProcessDie(rDieOpt->GetChild(), rDebugInfo, sName);
    ProcessDie(rDieOpt->GetSibling(), rDebugInfo, sParentName);
}

#else

////////////////////////////////////////////////////////////////////

static void ProcessDieIteratively(const boost::optional<CDebugInfoEntry>& rDieOpt, CDebugInfo& rDebugInfo)
{
    if (!rDieOpt) {
        return;
    }

    struct CNode
    {
        CNode(const CDebugInfoEntry& rDie, const std::string& sPrefix): m_Die(rDie), m_sPrefix(sPrefix) {}
        CDebugInfoEntry m_Die;
        std::string m_sPrefix;
    };

    std::stack<CNode> stack;
    stack.emplace(*rDieOpt, "");

    while (!stack.empty()) {
        CNode node = stack.top();
        const std::string sOldPrefix = node.m_sPrefix;
        node.m_Die.AddInfo(rDebugInfo, node.m_sPrefix);
        stack.pop();
        auto siblingOpt = node.m_Die.GetSibling();
        if (siblingOpt) {
            stack.emplace(*siblingOpt, sOldPrefix);
        }
        auto childOpt = node.m_Die.GetChild();
        if (childOpt) {
            stack.emplace(*childOpt, node.m_sPrefix);
        }
    }
}

#endif // #if defined(USE_RECURSION)

////////////////////////////////////////////////////////////////////

static CDebugInfo GetDebugInfoImpl(Dwarf_Debug dbg)
{
    CDebugInfo debugInfo;
    CCompilationUnitHeader cu(dbg);

    while (cu.MoveToNext()) {
        CDebugInfoEntry cuDie(dbg, cu.GetSibling());
#if defined(USE_RECURSION)
        ProcessDie(cuDie.GetChild(), debugInfo, "");
#else
        ProcessDieIteratively(cuDie.GetChild(), debugInfo);
#endif
    }
    return debugInfo;
}

namespace Typemock
{
namespace Linux
{

////////////////////////////////////////////////////////////////////

CDebugInfo CLinuxDebugInfoReader::GetDebugInfo(const std::string &sFileName)
{
    const int nFD = OpenFile(sFileName);

    BOOST_SCOPE_EXIT(&nFD) {
        close(nFD);
    } BOOST_SCOPE_EXIT_END

    Dwarf_Debug dbg = DwarfInit(nFD);

    BOOST_SCOPE_EXIT(&dbg) {
        Dwarf_Error err;
        (void) dwarf_finish(dbg, &err);
    } BOOST_SCOPE_EXIT_END

    return GetDebugInfoImpl(dbg);
}

} // namespace Linux
} // namespace Typemock
