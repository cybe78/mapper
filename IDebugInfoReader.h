#ifndef IDEBUGINFOREADER_H
#define IDEBUGINFOREADER_H

#include <string>
#include <vector>

#include "DebugInfo.h"

namespace Typemock
{

////////////////////////////////////////////////////////////////////

class IDebugInfoReader
{
public:
    virtual ~IDebugInfoReader() = default;

    static IDebugInfoReader* CreateInstance();
    virtual CDebugInfo GetDebugInfo(const std::string& sFileName) = 0;

protected:
    //Instances should only be created via CreateInstance() -
    //this is why this contructor is protected
    IDebugInfoReader() = default;
};

} // namespace Typemock

#endif // IDEBUGINFOREADER_H
