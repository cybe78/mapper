#ifndef MAPPER_H
#define MAPPER_H

#include <string>

#include "Exceptions.h"

namespace Typemock
{

////////////////////////////////////////////////////////////////////

class CMapper
{
public: // methods
    CMapper(int argc, char** argv);

    void Exec();

private: // methods
    void PrintHelp() const;
    std::string ParseParameters() const;

private: // fields
    int m_nArgc;
    char** m_ppArgv;
};

} // namespace Typemock

#endif // MAPPER_H
