#include "Precompiled.h"
#include "Mapper.h"

#include "IDebugInfoReader.h"

namespace  {
    std::string gVersion = "1.0.12";
}

////////////////////////////////////////////////////////////////////

static void CheckFileSize(const std::string& sFileName)
{
    try
    {
        const boost::uintmax_t MAX_FILE_SIZE = 20 * 1024 * 1024;
        if (boost::filesystem::file_size(sFileName) > MAX_FILE_SIZE) {
            std::ostringstream ss;
            ss << "The file size should not exceed " << MAX_FILE_SIZE << " bytes";
            throw Typemock::EMapperException(ss.str());
        }
    }
    catch(const boost::filesystem::filesystem_error& rFileSystemError)
    {
        std::ostringstream ss;
        ss << "Could not open the file: " << rFileSystemError.what();
        throw Typemock::EMapperException(ss.str());
    }
}

namespace Typemock
{

////////////////////////////////////////////////////////////////////

CMapper::CMapper(int argc, char **argv)
: m_nArgc(argc), m_ppArgv(argv)
{
}

////////////////////////////////////////////////////////////////////

void CMapper::Exec()
{
    const std::string sFilename = ParseParameters();

    if (!sFilename.empty())
    {
        CheckFileSize(sFilename);
    }
    boost::scoped_ptr<IDebugInfoReader> pReader(IDebugInfoReader::CreateInstance());
    if (pReader) {
        const CDebugInfo info = pReader->GetDebugInfo(sFilename);
        if (info.m_Functions.size() > 0) {
            std::cout << "Functions:\n\n";
            for (const auto& rFunction : info.m_Functions) {
                std::cout << rFunction.m_sName;
                if (rFunction.m_Addr.m_Lowpc != 0) {
                    std::cout << std::hex << " 0x" << rFunction.m_Addr.m_Lowpc << "+" << std::dec << rFunction.m_Addr.m_Highpc;
                }
                std::cout << "\n";
            }
        }
        if (info.m_ClassNames.size() > 0) {
            if (info.m_Functions.size() > 0) {
                std::cout << "\n\n";
            }
            std::cout << "Classes:\n\n";
            for (const std::string& sClassName : info.m_ClassNames) {
                std::cout << sClassName << "\n";
            }
        }
    }
    else {
        throw EMapperException("Cannot create pReader");
    }
}

////////////////////////////////////////////////////////////////////

void CMapper::PrintHelp() const
{
    std::cout << "mapper " << gVersion << "\n"
                 "Usage:\n"
                 "mapper [option]\n"
                 "or\n"
                 "mapper filename\n"
                 "where filename is the filename of a ELF file\n"
                 "Options:\n"
                 "-h or --help: this help message\n"
                 "-v or --version: version of this program\n";
}

////////////////////////////////////////////////////////////////////

std::string CMapper::ParseParameters() const
{
    std::string sFileName;

    if (m_nArgc < 2) {
        PrintHelp();
    }
    else if (m_nArgc == 2) {
        const std::string sOption = m_ppArgv[1];
        if ((sOption == "-h") || (sOption == "--help")) {
            PrintHelp();
        }
        else if ((sOption == "-v") || (sOption == "--version")) {
            std::cout << gVersion << std::endl;
        }
        else {
            sFileName = sOption;
        }
    }
    else {
        throw EMapperException("Unexpected number of arguments");
    }

    return sFileName;
}

} // namespace Typemock
