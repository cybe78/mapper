#include "Precompiled.h"
#include "DebugInfoEntry.h"

#include "Exceptions.h"
#include "FunctionInfo.h"

static void AppendName(std::string& sParent, const std::string& sChild)
{
    if (sChild.empty()) {
        return;
    }
    if (!sParent.empty()) {
        sParent += "::";
    }
    sParent += sChild;
}

namespace Typemock
{
namespace Linux
{
namespace Details
{

////////////////////////////////////////////////////////////////////

static Dwarf_Addr GetAddrForAttr(Dwarf_Attribute attr)
{
    Dwarf_Half form = 0;
    Dwarf_Error err = nullptr;
    Dwarf_Unsigned offset = 0;
    Dwarf_Addr addr = 0;

    dwarf_whatform(attr, &form, &err);

    switch(form)
    {
        case DW_FORM_addr:
            if (dwarf_formaddr(attr, &addr, &err) != DW_DLV_OK) {
                throw EMapperException("GetAddrForAttr(): dwarf_formaddr()");
            }
            break;
        case DW_FORM_data8:
            if (dwarf_formudata(attr, &offset , &err) != DW_DLV_OK) {
                throw EMapperException("GetAddrForAttr(): dwarf_formudata()");
            }
            addr = offset;
            break;
        default:
            break;
    }
    return addr;
}

////////////////////////////////////////////////////////////////////

CDebugInfoEntry::CDebugInfoEntry(Dwarf_Debug dbg, Dwarf_Die die): m_Dbg(dbg), m_DIE(die) {}

////////////////////////////////////////////////////////////////////

boost::optional<CDebugInfoEntry> CDebugInfoEntry::GetChild() const
{
    Dwarf_Die childDie = nullptr;
    Dwarf_Error err;

    int nRC = dwarf_child(m_DIE, &childDie, &err);
    if (nRC == DW_DLV_ERROR) {
        throw EMapperException("Error getting child of DIE");
    }
    if (nRC == DW_DLV_NO_ENTRY) {
        return boost::none;
    }
    return CDebugInfoEntry(m_Dbg, childDie);
}

////////////////////////////////////////////////////////////////////

boost::optional<CDebugInfoEntry> CDebugInfoEntry::GetSibling() const
{
    Dwarf_Die sibling = nullptr;
    Dwarf_Error err;

    int rc = dwarf_siblingof(m_Dbg, m_DIE, &sibling, &err);

    if (rc == DW_DLV_ERROR) {
        throw EMapperException("Error getting sibling of DIE");
    }
    else if (rc == DW_DLV_NO_ENTRY) {
        return boost::none;
    }
    return CDebugInfoEntry(m_Dbg, sibling);
}

////////////////////////////////////////////////////////////////////

boost::optional<std::string> CDebugInfoEntry::GetName() const
{
    char* dieName = nullptr;
    Dwarf_Error err;
    const int rc = dwarf_diename(m_DIE, &dieName, &err);
    if (rc == DW_DLV_ERROR) {
        throw EMapperException("Error in dwarf_diename");
    }
    if (rc == DW_DLV_NO_ENTRY) {
        return boost::none;
    }
    return std::string(dieName);
}

////////////////////////////////////////////////////////////////////

CFuncAddr CDebugInfoEntry::GetAddress() const
{
    Dwarf_Error err;
    Dwarf_Attribute* pAttrs = nullptr;
    Dwarf_Addr lowpc = 0;
    Dwarf_Addr highpc = 0;
    Dwarf_Signed nAttrCount = 0;

    if (dwarf_attrlist(m_DIE, &pAttrs, &nAttrCount, &err) != DW_DLV_OK) {
        throw EMapperException("GetAddress(): dwarf_attlist()");
    }

    for (int i = 0; i < nAttrCount; ++i) {
        Dwarf_Half attrcode = 0;
        if (dwarf_whatattr(pAttrs[i], &attrcode, &err) != DW_DLV_OK) {
            throw EMapperException("GetAddress(): dwarf_whatattr()");
        }
        if (attrcode == DW_AT_low_pc) {
            lowpc = GetAddrForAttr(pAttrs[i]);
        }
        else if (attrcode == DW_AT_high_pc) {
            highpc = GetAddrForAttr(pAttrs[i]);
        }
    }
    return CFuncAddr(lowpc, highpc);
}

////////////////////////////////////////////////////////////////////

void CDebugInfoEntry::ProcessTagSubprogram(CDebugInfo &rInfo, const std::string &sPrefix) const
{
    auto nameOpt = GetName();
    if (nameOpt) {
        std::string sName = sPrefix;
        AppendName(sName, *nameOpt);
        rInfo.m_Functions.emplace_back(CFunctionInfo(sName, GetAddress()));
    }
}

////////////////////////////////////////////////////////////////////

void CDebugInfoEntry::AddInfo(CDebugInfo &rInfo, std::string &sPrefix) const
{
    const Dwarf_Half tag = GetTag();
    switch (tag) {
        case DW_TAG_subprogram:
            ProcessTagSubprogram(rInfo, sPrefix);
            break;
        case DW_TAG_namespace:
            ProcessTagNamespace(sPrefix);
            break;
        case DW_TAG_class_type:
            ProcessTagClass(rInfo, sPrefix);
            break;
        default:
            break;
    }
}

////////////////////////////////////////////////////////////////////

void CDebugInfoEntry::ProcessTagClass(CDebugInfo &rInfo, std::string &sPrefix) const
{
    auto nameOpt = GetName();
    if (!nameOpt) {
        throw EMapperException("Class does not have name");
    }
    AppendName(sPrefix, *nameOpt);
    rInfo.m_ClassNames.push_back(sPrefix);
}

////////////////////////////////////////////////////////////////////

void CDebugInfoEntry::ProcessTagNamespace(std::string &sPrefix) const
{
    auto nameOpt = GetName();
    if (!nameOpt) {
        nameOpt = "_anonymous_namespace_";
    }
    AppendName(sPrefix, *nameOpt);
}

////////////////////////////////////////////////////////////////////

Dwarf_Half CDebugInfoEntry::GetTag() const
{
    Dwarf_Half tag;
    Dwarf_Error err;

    if (dwarf_tag(m_DIE, &tag, &err) != DW_DLV_OK) {
        throw EMapperException("Error in dwarf_tag");
    }
    return tag;
}

} // namespace Details
} // namespace Linux
} // namespace Typemock
