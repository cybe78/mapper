#ifndef LINUXDEBUGINFOREADER_H
#define LINUXDEBUGINFOREADER_H

#include <string>

#include "DebugInfo.h"
#include "IDebugInfoReader.h"

namespace Typemock
{
namespace Linux
{

class CLinuxDebugInfoReader: public IDebugInfoReader
{
public:
    CLinuxDebugInfoReader() = default;

    CDebugInfo GetDebugInfo(const std::string& sFileName) override;
};

} // namespace Linux
} // namespace Typemock

#endif // LINUXDEBUGINFOREADER_H
