#include "Precompiled.h"
#include "CompilationUnitHeader.h"

#include "Exceptions.h"

namespace Typemock
{
namespace Linux
{
namespace Details
{

////////////////////////////////////////////////////////////////////

CCompilationUnitHeader::CCompilationUnitHeader(Dwarf_Debug dbg): m_Dbg(dbg), m_Next(0) {}

////////////////////////////////////////////////////////////////////

bool CCompilationUnitHeader::MoveToNext()
{
    Dwarf_Unsigned cu_header_length = 0, abbrev_offset = 0;
    Dwarf_Half version_stamp = 0, address_size = 0;
    Dwarf_Error err;

    int rc = dwarf_next_cu_header(
                m_Dbg,
                &cu_header_length,
                &version_stamp,
                &abbrev_offset,
                &address_size,
                &m_Next,
                &err);

    if (rc == DW_DLV_ERROR) {
        throw EMapperException("Error reading DWARF cu header");
    }

    return rc != DW_DLV_NO_ENTRY;
}

////////////////////////////////////////////////////////////////////

Dwarf_Die CCompilationUnitHeader::GetSibling() const
{
    Dwarf_Die sibling = nullptr;
    Dwarf_Die noDie = nullptr;
    Dwarf_Error err;

    /* Expect the CU to have a single sibling - a DIE */
    if (dwarf_siblingof(m_Dbg, noDie, &sibling, &err) == DW_DLV_ERROR) {
        throw EMapperException("Error getting sibling of CU");
    }
    return sibling;
}

} // namespace Details
} // namespace Linux
} // namespace Typemock
