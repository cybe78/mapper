#ifndef DEBUGINFO_H
#define DEBUGINFO_H

#include <string>
#include <vector>

#include "FunctionInfo.h"

namespace Typemock
{

struct CDebugInfo
{
    std::vector<CFunctionInfo> m_Functions;
    std::vector<std::string> m_ClassNames;
};

} // namespace Typemock

#endif // DEBUGINFO_H
