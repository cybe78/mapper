#ifndef FUNCTIONINFO_H
#define FUNCTIONINFO_H

#include <string>

#include "FuncAddr.h"

namespace Typemock
{

struct CFunctionInfo
{
    CFunctionInfo(const std::string& sName, const CFuncAddr& rAddr): m_sName(sName), m_Addr(rAddr) {}
    std::string m_sName;
    CFuncAddr m_Addr;
};

} // namespace Typemock

#endif // FUNCTIONINFO_H
