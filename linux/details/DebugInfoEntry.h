#ifndef DEBUGINFOENTRY_H
#define DEBUGINFOENTRY_H

#include "DebugInfo.h"
#include "FuncAddr.h"

#include <boost/optional.hpp>
#include <libdwarf.h>
#include <string>

namespace Typemock
{
namespace Linux
{
namespace Details
{

class CDebugInfoEntry
{
public:
    CDebugInfoEntry(Dwarf_Debug dbg, Dwarf_Die die);

    boost::optional<CDebugInfoEntry> GetChild() const;
    boost::optional<CDebugInfoEntry> GetSibling() const;
    void AddInfo(CDebugInfo& rInfo, std::string& sPrefix) const;

private: // private methods

    boost::optional<std::string> GetName() const;
    CFuncAddr GetAddress() const;
    void ProcessTagSubprogram(CDebugInfo& rInfo, const std::string& sPrefix) const;
    void ProcessTagClass(CDebugInfo& rInfo, std::string& sPrefix) const;
    void ProcessTagNamespace(std::string& sPrefix) const;
    Dwarf_Half GetTag() const;

private: // private fields
    Dwarf_Debug m_Dbg;
    Dwarf_Die m_DIE;
};

} // namespace Details
} // namespace Linux
} // namespace Typemock

#endif // DEBUGINFOENTRY_H
