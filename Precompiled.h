// Add C includes here

#if defined(linux)
#include <dwarf.h>
#include <fcntl.h>
#include <libdwarf.h>
#endif

#if defined __cplusplus

// Add C++ includes here

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <stack>

#include <boost/filesystem.hpp>
#include <boost/filesystem/exception.hpp>
#include <boost/optional.hpp>
#include <boost/scope_exit.hpp>
#include <boost/scoped_ptr.hpp>

#endif // #if defined __cplusplus
