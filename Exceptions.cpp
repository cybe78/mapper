#include "Precompiled.h"
#include "Exceptions.h"

namespace Typemock
{

////////////////////////////////////////////////////////////////////

EMapperException::EMapperException(const std::string &sErrorMessage)
: std::logic_error(sErrorMessage)
{
}

} // namespace Typemock
