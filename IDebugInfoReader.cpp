#include "Precompiled.h"
#include "IDebugInfoReader.h"

#if defined(linux)
#include "linux/LinuxDebugInfoReader.h"
#elif defined(_WIN32)
#static_assert(false, "Windows is not supported yet");
#else
#static_assert(false, "Unexpected OS");
#endif

namespace Typemock
{

////////////////////////////////////////////////////////////////////

IDebugInfoReader *IDebugInfoReader::CreateInstance()
{
#if defined(linux)
    return new Linux::CLinuxDebugInfoReader();
#endif
}

} // namespace Typemock
