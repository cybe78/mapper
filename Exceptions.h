#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <stdexcept>

namespace Typemock
{

class EMapperException: public std::logic_error
{
public:
    EMapperException(const std::string& sErrorMessage);
};

} // namespace Typemock

#endif // EXCEPTIONS_H
