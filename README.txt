Author: Ruslan Minyukov

The program, named mapper, extracts and prints the list of functions and classes used by the specified ELF.

Program options
----------------
-h --help
-v --version

The required argument is the name of a ELF file.

Compilation
---------------
The solution can be built by CMake or qmake.

3rd party libraries, listed below in the appropriate section of this document, should be installed.

I compiled the solution in Ubuntu 16.04.6 LTS 64-bit.

How I installed 3rd party libraries: installed the following packages with apt-get:

libelf-dev libdwarf-dev libboost-all-dev


Solution
---------------
The main class is CMapper to which the program arguments are passed.
It parses the program arguments, prints the help message if -h or --help is specified,
or prints the program version if -h or --help is specified. If only the filename is specified,
it passes the filename to an instance of class derived from IDebugInfoReader. The exact class
depends on the operating system. If the program is compiled for Linux, CLinuxDebugInfoReader is used.
The debug info reader checks if the file size does not exceed 20 Mb. If the file size is OK,
it initializes the Dwarf library, and extracts functions and classes with its help, and passes back to
CMapper. The latter prints the received information.

Let's describe how the debug info reader extracts this information. The debug info reader
iterates through the compilation units. Each compilation unit contains the debug information represented
as a tree of DIEs; DIE stands for 'debug information entry'. Via traversing the tree, the list of
functions and the list of classes are created: while traversing, if DIE is related to a function
 - it does not matter if it is a member function or not - the name of that function is added to the appropriate
list. The names of classes are extracted the same way. Also, if the DIE is related to a function, its address
is extracted as well: DIE can contain a list of attributes; the address is represented as the 'lowpc' attribute,
the offset is represented as the 'highpc' attribute. The name of function includes the name of namespace if
the function is defined in that namespace. The name of a member function includes the name of its class.
If a class is defined in a namespace, the name of that class will follow the name of the namespace.
Here are the examples:

Foo()
MyNamespace::Function()
MyClass::Method()
MyNamespace::MyClass::Method()

Examples of the names of classes:

MyClass
MyNamespace::MyClass

If the namespace is anonymous, its name is printed as '_anonymous_namespace_'.

To form the correct prefix of a function name, the std::string& parameter is passed
into the recursion function, named ProcessDie.

The recursion function ProcessDie() cannot cause the stack overflow because it is a tail recursion;
a tail recursion can be transformed into a cycle by the compiler.


3rd party libraries
--------------------
- elfutils

  License:

  the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 3 of the License, or (at
  your option) any later version
  
  or
  
  the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at
  your option) any later version

  or

  both in parallel

- dwarf

  License: LGPL 2.1

- Boost

  License: Boost Software License


