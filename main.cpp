#include "Precompiled.h"
#include "Mapper.h"

int main(int argc, char** argv)
{
    Typemock::CMapper mapper(argc, argv);

    try
    {
        mapper.Exec();
    }
    catch (const Typemock::EMapperException& rException)
    {
        std::cerr << "Error: " << rException.what() << std::endl;
    }
    return 0;
}
