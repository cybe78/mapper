#ifndef FUNCADDR_H
#define FUNCADDR_H

namespace Typemock
{

using ULL = unsigned long long;

struct CFuncAddr
{
    CFuncAddr(ULL lowpc, ULL highpc): m_Lowpc(lowpc), m_Highpc(highpc) {}
    ULL m_Lowpc;
    ULL m_Highpc;
};

} // namespace Typemock

#endif // FUNCADDR_H
