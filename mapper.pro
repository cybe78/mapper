TEMPLATE = app
CONFIG += console c++11 precompile_header
CONFIG -= app_bundle
CONFIG -= qt
unix:!macx: LIBS += -ldwarf -lelf -lboost_filesystem -lboost_system

SOURCES += \
        Exceptions.cpp \
        IDebugInfoReader.cpp \
        linux/LinuxDebugInfoReader.cpp \
        Mapper.cpp \
        linux/details/CompilationUnitHeader.cpp \
        linux/details/DebugInfoEntry.cpp \
        main.cpp

HEADERS += \
    DebugInfo.h \
    Exceptions.h \
    FuncAddr.h \
    FunctionInfo.h \
    IDebugInfoReader.h \
    LinuxDebugInfoReader.h \
    Mapper.h \
    linux/LinuxDebugInfoReader.h \
    linux/details/CompilationUnitHeader.h \
    linux/details/DebugInfoEntry.h \
    linux/details/FuncAddr.h \
    linux/details/FunctionInfo.h

PRECOMPILED_HEADER = Precompiled.h

precompile_header:!isEmpty(PRECOMPILED_HEADER) {
  DEFINES += USING_PCH
}
